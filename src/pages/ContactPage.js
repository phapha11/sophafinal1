import React from 'react'
import "../styles/ContactPage.css"
import {FiPhoneCall} from 'react-icons/fi'
import {AiOutlineMail} from 'react-icons/ai'
import {BsFacebook} from 'react-icons/bs'
import {AiFillInstagram} from 'react-icons/ai'
import {BsYoutube} from 'react-icons/bs'
import Map from '../image/Screenshot 2023-12-25 223831.png'
const ContactPage = () => {
  return (
    <div className='container'>
       <h1 className=' mt-5 mx-5'>Contact us</h1>

         <div className='d-flex flex-column container flex-wrap flex-md-row justify-content-center gap-5 mt-5'>
            <div className='item-cardxx px-4 pb-3  col-sm-12 col-md-4 col-lg-3 '
            style={{ backgroundColor: 'rgb(248,248,248)'}}
            >
                 <h5 className='pt-4 px-4'>Call our hotline to place an order or talk to our customer service team:</h5>
                <div className='pt-4 px-4 ' ><FiPhoneCall /><span>0962012953</span></div> 
                <div className='pt-2 px-4 ' ><AiOutlineMail /><span>redapron@gmail.com.kh</span></div> 
                <h5 className='pt-4 px-4'><strong>Follow us on</strong></h5>
                <div className='pt-3 px-4 ' ><BsFacebook /><span>Facebook</span></div> 
                <div className='pt-3 px-4 ' ><AiFillInstagram/><span>Instagram</span></div> 
                <div className='pt-3 px-4 ' ><BsYoutube /><span>Youtube</span></div>
                <h5 className='pt-4 px-4'><strong>Chat with us</strong></h5> 
                <div className='pt-3 px-4 ' ><BsFacebook /><span>Facebook</span></div> 
                <div className='pt-3 px-4 ' ><AiFillInstagram/><span>Instagram</span></div> 
            </div>
            <div className='item-cardxx px-4 pb-3  col-sm-12 col-md-4 col-lg-3 bg-light'>
            <h3 className='pt-4 px-4'><strong>Have Question?</strong></h3>
            <div className='pt-1 px-4 ' ><span>Prefer to send us a message? We’ll get right back to you:</span></div> 
            <div class="form-container pt-5">
                <form>
                  
                  <input  type="text" id="input" placeholder='Your name' />
                  <input className='mt-2' type="text" id="input" placeholder='Your email address' />
                  <input className='mt-2' type="text" id="input" placeholder='Your Phone' />
                  <textarea name="" id="" cols="60" rows="5" placeholder='Write Your message'></textarea>
                  <button type="submit" className='mt-4 ' style={{width:"500px",height:"50px"}} >Submit</button>
                </form>
              </div>
            </div>

         </div>

        <img src={Map} alt="" className='Map'/>
    </div>
  )
}

export default ContactPage
