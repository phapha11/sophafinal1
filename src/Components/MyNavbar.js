import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import Logo from "../image/logored-removebg-preview.png"
import "../styles/MyNavbar.css"
const MyNavbar = () => {
  return (
    <div>
       <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <NavLink to="/" className="logo d-flex justify-content-center align-items-center "><img src={Logo} alt="" /></NavLink>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto text-center">
            <NavLink className="nav-link"  to="/">Home</NavLink>
            <NavLink className="nav-link" to="/service">About</NavLink>
            <NavLink className="nav-link" to="/menu">Menu</NavLink>
            <NavLink className="nav-link" to="/contact">Contact</NavLink>
          </Nav>
          <div className='d-flex justify-content-center'>
               <button className='btn btn-warning me-3'>Login</button>
               <button className='btn btn-success'>Sign Up</button>
          </div>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    
    </div>
  )
}

export default MyNavbar
